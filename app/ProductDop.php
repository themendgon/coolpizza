<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDop extends Model
{


    protected $table = 'dop_pizza';
    protected $fillable = [
        'order_product_id','dop_id',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public function order_product()
    {
        return $this->belongsTo('App\OrderProduct');
    }
    public function dop()
    {
        return $this->belongsTo('App\Dop');
    }

}