<?php

namespace App\Http\Requests;
use Illuminate\Contracts\Validation\Validator;


use Illuminate\Foundation\Http\FormRequest;

class MapRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lat'=> 'required',
            'lon'=> 'required'
        ];
    }

    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }
}