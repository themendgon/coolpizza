<?php

namespace App\Http\Controllers\Admin;

use App\Dop;
use App\Http\Controllers\Controller;
use App\Http\Requests\DopRequest;
use Carbon\Carbon;

class DopController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $dop = Dop::get();
        return view('admin.catalog.dop.index')->with(compact('dop'));
    }
    public function create()
    {
        return view('admin.catalog.dop.create');
    }
    public function show($id)
    {
        $post = Dop::find($id);
        return view('admin.catalog.dop.edit', compact('post','id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DopRequest $request)
    {
        Dop::insert([
            'title' =>  $request['title'],
            'prise' => $request['prise'],
            'created_at'=> Carbon::now()->toDateTimeString(),
            'updated_at'=> Carbon::now()->toDateTimeString(),
        ]);
        return redirect()->route('admin.catalog.dop.index')->with(['message' => 'Data save']);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function update(DopRequest $request, $id)
    {
        Dop::where('id',$id)
            ->update([
                'title' =>  $request['title'],
                'prise' => $request['prise'],
                'updated_at'=> Carbon::now()->toDateTimeString(),
            ]);
        return redirect()->route('admin.catalog.dop.index')->with('success','User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public  function delete($id)
    {
         Dop::deleted($id);
        return redirect()->back();
    }


}