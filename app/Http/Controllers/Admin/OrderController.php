<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StatusRequest;
use App\Order;
use Carbon\Carbon;



class OrderController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $all=Order::with('list.product')->with('user')->with('list.dop.dop')->get();
        return view('admin.orders.index')->with(compact('all'));
    }

    public function show($id)
    {
        $post = Order::find($id);
        return view('admin.orders.edit', compact('post'));
    }



    public function update(StatusRequest $request, $id)
    {
        Order::where('id', $id)
            ->update([
                'status'=>$request->status,
                'updated_at'=> Carbon::now()->toDateTimeString(),
            ]);
        return redirect()->route('admin.order.index')->with('success','User updated successfully');
    }


}