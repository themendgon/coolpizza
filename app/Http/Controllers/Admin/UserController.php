<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserAdminRequest;
use App\Http\Requests\UserEditRequest;
use App\Mail\MailNotification;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\User;


class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function start()
    {
        return view('admin.admin');
    }
    public function index()
    {
        $admins = User::where('role', 0)
            ->paginate(2, ["*"], "admin");
        $managers = User::where('role', 1)
            ->paginate(2, ["*"], "manager");
        $users = User::where('role', 2)
            ->paginate(2, ["*"], "user");
        $bans = User::onlyTrashed()
            ->paginate(2, ["*"], "ban");

        return view('admin.users.index', compact('admins','managers','users','bans'));
    }
    public function create()
    {
        return view('admin.users.create');
    }
    public function show($id)
    {
        $post = User::where('id', $id)->first();
        return view('admin.users.edit', ['users' => $post],['id' => $id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserAdminRequest $request)
    {
        $random = str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890!$%^&!$%^&');
        $password = substr($random, 0, 10);
        $user=collect();
        $user->put('password',$password);
        $user->put('email',$request['email']);
        $user->put('name',$request['name']);
        User::insert([
            'name' =>  $request['name'],
            'email' => $request['email'],
            'phone'=> $request['phone'],
            'address'=> $request['address'],
            'created_at'=> Carbon::now()->toDateTimeString(),
            'updated_at'=> Carbon::now()->toDateTimeString(),
            'email_verified_at'=> Carbon::now()->toDateTimeString(),
            'role'=> $request['role'],
            'password' => Hash::make($password),
        ]);
        Mail::to($request['email'])->send(new MailNotification($user));
        return redirect()->route('admin.users.index')->with(['message' => 'Data save']);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function update(UserEditRequest $request, $id)
    {
        User::where('id', $id)
            ->update([
                'name' =>  $request['name'],
                'email' => $request['email'],
                'phone'=> $request['phone'],
                'address'=> $request['address'],
                'role'=> $request['role'],
                'updated_at'=> Carbon::now()->toDateTimeString(),
                'email_verified_at'=> Carbon::now()->toDateTimeString(),
                'password' => Hash::make($request['password']),
            ]);
        Mail::to($request['email'])->send(new MailNotification($request));
        return redirect()->route('admin.users.index')->with('success','User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $baned = User::withTrashed()->findOrFail($id);
        $baned->forceDelete();
        return redirect()->route('admin.users.index');
    }
    public function ban($id)
    {
        User::destroy($id);
        return redirect()->route('admin.users.index');
    }
    public function unban($id)
    {
        User::withTrashed()
            ->where('id', $id)
            ->restore();
        return redirect()->route('admin.users.index');
    }
}