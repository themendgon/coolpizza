<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductReguest;
use App\Product;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;


class PizzaController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pizza = Product::where('pizza',true)->get();
        $drink= Product::Where('pizza',false)->get();
        $deletes = Product::onlyTrashed()->get();
        return view('admin.catalog.index')->with(compact('pizza','drink','deletes'));
    }
    public function create()
    {
        return view('admin.catalog.create');
    }

    public function show($id)
    {
        $post = Product::find($id);
        return view('admin.catalog.edit', compact('post','id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductReguest $request)
    {

        $imageName=Product::addFile( $request->file('image'),$request->image);
        Product::insert([
            'title' =>  $request['title'],
            'prise' => $request['prise'],
            'parts' => $request['parts'],
            'sale'=> $request->get('sale', 0),
            'hot'=> $request->get('hot', 0),
            'pizza'=> $request->get('pizza', 0),
            'image'=> $imageName,
            'vegetarian'=> $request->get('vegetarian', 0),
            'created_at'=> Carbon::now()->toDateTimeString(),
            'updated_at'=> Carbon::now()->toDateTimeString(),
        ]);
        return redirect()->route('admin.catalog.index')->with(['message' => 'Data save']);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(ProductReguest $request, $id)
    {
        $product=Product::find($id);
        $url='image/'.$product->image;
        Storage::delete($url);
        $imageName=Product::addFile( $request->file('image'),$request->image);
        Product::where('id', $id)
            ->update([
                'title' =>  $request['title'],
                'prise' => $request['prise'],
                'parts' => $request['parts'],
                'sale'=> $request->get('sale', 0),
                'hot'=> $request->get('hot', 0),
                'pizza'=> $request->get('pizza', 0),
                'image'=> $imageName,
                'vegetarian'=> $request->get('vegetarian', 0),
                'updated_at'=> Carbon::now()->toDateTimeString(),
            ]);
        return redirect()->route('admin.catalog.index')->with('success','User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public  function delete($id)
    {
        $file= Product::find($id);
        $url='image/'.$file->image;
        Storage::delete($url);
        $baned = Product::withTrashed()->findOrFail($id);
        $baned->forceDelete();
        return redirect()->back();
    }
    public function ban($id)
    {
        Product::destroy($id);
        return redirect()->back();
    }

    public function unban($id)
    {
        Product::withTrashed()
            ->where('id', $id)
            ->restore();
        return redirect()->back();
    }
}