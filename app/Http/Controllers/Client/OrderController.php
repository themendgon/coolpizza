<?php

namespace App\Http\Controllers\Client;

use App\Dop;
use App\Http\Controllers\Controller;
use App\Http\Requests\GuestRequest;
use App\Http\Requests\MapRequest;
use App\Order;
use App\OrderProduct;
use App\Product;
use App\ProductDop;
use App\User;
use Carbon\Carbon;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Stripe;
use Session;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $amount=0;
        $all=session()->get('cart',[]);
        $products=collect();
        $dop=array();
        foreach ($all as $i=>$item) {
            if (isset($item)) {
                array_push($dop,$product = Product::find($item['product']));
                $amount += ($product->prise * $item['size']);
                if (isset($item['dop'])) {
                    foreach ($item['dop'] as  $value) {
                        array_push($dop, $amount_dop = Dop::find($value));
                        $amount += $amount_dop->prise;
                    }
                }
                $products->put($i, $dop);
                $dop = array();
            }
        }
        session()->put('prise', $amount);
        return view('client.orders.index',compact('products'));
    }

    public function create(Request $request)
    {
        $cart = session()->get('cart');
        if(isset($cart))
        {
            $count=count($cart);
        }
        else{
            $count=0;
        }
        if($request->pizza==null)
        {
            $request->pizza=$request->drink;
            $request->size=1;
        }
        $prise = session()->get('prise',0);
        $product=Product::find($request->pizza);
        $prise+=$product->prise*$request->size;
        $all=Dop::latest()->first()->id;
        $dop=Dop::get();
        $i=0;
        session()->put('cart.'.$count.'.product',$request->pizza);
        session()->put('cart.'.$count.'.size',$request->size);
        while($i!=$all)
        {
            if($request[$i]!=null)
            {
                $prise+=$dop[$i]->prise;
                session()->push('cart.'.$count.'.dop', $request[$i]);
            }
            $i++;
        }
        session()->put('prise', $prise);
        session()->save();
         return redirect()->route('client.index')->with(['status' => 'Product add']);
    }

    public function show($id)
    {
        $post = Product::find($id);
        return view('client.catalog.edit', compact('post','id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $all=session()->get('cart');
        $user=Auth::id();
        if(empty($all)) redirect()->back();
        $order=Order::create([
            'prise' =>session()->get('prise'),
            'status' =>'create',
            'cash'=>session()->get('pay'),
            'lat'=>session()->get('lat'),
            'lon'=>session()->get('lon'),
            'user_id' =>$user,
            'created_at'=> Carbon::now()->toDateTimeString(),
            'updated_at'=> Carbon::now()->toDateTimeString(),
        ]);
        foreach ($all as $i=>$item)
        {
            $product=OrderProduct::create([
                'order_id' => $order->id,
                'product_id' => $item['product'],
                'size' => $item['size'],
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ]);
            if(isset($item['dop'])) {
                foreach ($item['dop'] as $key => $value) {
                        ProductDop::insert([
                            'order_product_id' => $product->id,
                            'dop_id' => $value,
                            'created_at' => Carbon::now()->toDateTimeString(),
                            'updated_at' => Carbon::now()->toDateTimeString(),
                        ]);

                }
            }
        }
        session()->forget('cart');
        session()->forget('prise');
        session()->save();
        return redirect()->route('client.index')->with(['message' => 'Data save']);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function guest(GuestRequest $request)
    {
        $random = str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890!$%^&!$%^&');
        $email = substr($random, 0, 5);
        if($request['email']==null)
        {
            $request['email']= $email.'@gmail.com';
        }
        event(new Registered($user = User::create([
                'name' =>  $request['name'],
                'email' => $request['email'],
                'phone'=> $request['phone'],
                'address'=> $request['address'],
                'created_at'=> Carbon::now()->toDateTimeString(),
                'updated_at'=> Carbon::now()->toDateTimeString(),
                'email_verified_at'=> Carbon::now()->toDateTimeString(),
                'role'=> 2,
                'password' => 1,
            ])));
        $this->guard()->login($user);

        session()->put('lat',$request->lat);
        session()->put('lon',$request->lon);
        if($request['cash']==1)
        {
            session()->put('pay',1);
            return redirect()->route('client.order.store');
        }
        else
            {
            session()->put('pay',0);
            return redirect()->route('client.order.payment');
            }
    }
    public function user(MapRequest $request)
    {
        session()->put('lat',$request->lat);
        session()->put('lon',$request->lon);
        if($request['cash']==1){
            session()->put('pay',1);
            return redirect()->route('client.order.store');}
        else
            session()->put('pay',0);
        return redirect()->route('client.order.payment');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public  function delete($id)
    {
        session()->forget('cart.'.$id);

        return redirect()->back()->with(['status'=>'Delete list']);
    }
    public function payment()
    {
        return view('client.orders.pay');
    }

    public function post(Request $request)
    {
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        Stripe\Charge::create ([
            "amount" => session()->get('prise') * 100,
            "currency" => "usd",
            "source" => $request->stripeToken,
            "description" => "Test payment from itsolutionstuff.com."
        ]);
        Session::flash('success', 'Payment successful!');

        return redirect()->route('client.order.store');
    }
    protected function guard()
    {
        return Auth::guard();
    }
}