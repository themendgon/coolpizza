<?php

namespace App\Http\Controllers\Client;

use App\Dop;
use App\Http\Controllers\Controller;
use App\Http\Requests\SearchRequest;
use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;



class CatalogController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pizza = Product::where('pizza',true)->sortable()->get();
        $dop=Dop::get();
        $drink= Product::Where('pizza',false)->sortable()->get();
        return view('client.catalog.index')->with(compact('pizza','drink','dop'));
    }

    public function search(SearchRequest $request)
    {
        $dop=Dop::get();
        $pizza = Product::where('title',$request->search)->get();
        return view('client.catalog.search')->with(compact('pizza','dop'));
    }

    public function pizza()
    {
        $dop=Dop::get();
        $pizza = Product::where('pizza',true)->sortable()->get();
        return view('client.catalog.pizza')->with(compact('pizza','dop'));
    }

    public function drink()
    {
        $drink= Product::Where('pizza',false)->sortable()->get();
        return view('client.catalog.drink')->with(compact('drink'));
    }

    public function check(Request $request)
    {
        $product=Product::where('pizza',true)->get();

        if ($request->data[0] == 1) {
            $product=$product->where('hot',true);
        }
        if ($request->data[1] == 1) {
            $product=$product->where('Vegetarian',true);
        }
        if ($request->data[2] == 1) {
            $product=$product->where('Sale ',true);
        }
        return $product;
    }

    public function show($id)
    {
        $post = Product::find($id);
        return view('client.catalog.edit', compact('post','id'));
    }
    public function map()
    {
        $order=Order::where('user_id',Auth::id())->get();
        return view('client.map')->with(compact('order'));
    }
}