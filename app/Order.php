<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{


    protected $table = 'order';
    protected $fillable = [
        'prise', 'status','user_id','lat','lon','cash'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function list()
    {
        return $this->hasMany('App\OrderProduct');
    }

}