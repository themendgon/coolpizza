<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{


    protected $table = 'order_product';
    protected $fillable = [
         'product_id','order_id','size',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }
    public function product()
    {
        return $this->belongsTo('App\Product');
    }
    public function dop()
    {

        return  $this->hasMany('App\ProductDop');
    }

}