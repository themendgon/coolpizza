<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    use Sortable;
    use SoftDeletes;
    protected $table = 'product';
    protected $fillable = [
        'title', 'prise', 'image','parts','sale','hot','rating','vegetarian',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public $sortable = ['title', 'prise','parts', 'created_at', 'updated_at'];
    protected $dates = ['deleted_at'];
    public static  function addFile($file,$image)
    {
        $imageName = time().'.'.$image->getClientOriginalExtension();
        Storage::putFileAs(
            'image', $file, $imageName
        );
        return $imageName;
    }
    public function getImageUrlAttribute() {
        return Storage::url('image/'.$this->image);
    }
}
