<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('title');
            $table->integer('prise');
            $table->char('image');
            $table->text('parts')->nullable();
            $table->boolean('pizza');
            $table->boolean('sale')->default(false);
            $table->boolean('hot')->default(false);
            $table->integer('rating')->default(0);
            $table->boolean('vegetarian')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pizza');
    }
}
