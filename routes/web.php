<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function() {
    //Logged-in user with the extended group
    Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('Admin')->group(function () {
        Route::prefix('users')->name('users.')->group(function () {
            Route::get('/index', 'UserController@index')->name('index');
            //add
            Route::get('create', 'UserController@create')->name('create');//list function add edit remove
            Route::post('/store', 'UserController@store')->name('store');//list function add edit remove
            //edit
            Route::get('/show/{id}', 'UserController@show')->name('show');//update page
            Route::post('/update/{id}', 'UserController@update')->name('update');//update
            //remove
            Route::get('/destroy/{id}', 'UserController@destroy')->name('destroy');//remove
            //ban
            Route::get('/ban/{id}', 'UserController@ban')->name('ban');//banned
            //unban
            Route::get('/unban/{id}', 'UserController@unban')->name('unban');//unbanned
        });
        Route::prefix('catalog')->name('catalog.')->group(function () {
            Route::get('/index', 'PizzaController@index')->name('index');

            Route::get('create', 'PizzaController@create')->name('create');//list function add edit remove
            Route::post('/store', 'PizzaController@store')->name('store');//list function add edit remove
            //edit
            Route::get('/show/{id}', 'PizzaController@show')->name('show');//update page
            Route::post('/update/{id}', 'PizzaController@update')->name('update');//update
            //remove
            Route::get('/delete/{id}', 'PizzaController@delete')->name('delete');//remove
            //ban
            Route::get('/ban/{id}', 'PizzaController@ban')->name('ban');//banned
            //unban
            Route::get('/unban/{id}', 'PizzaController@unban')->name('unban');//unbanned
            Route::prefix('dop')->name('dop.')->group(function () {
                Route::get('/index', 'DopController@index')->name('index');

                Route::get('create', 'DopController@create')->name('create');//list function add edit remove
                Route::post('/store', 'DopController@store')->name('store');//list function add edit remove
                //edit
                Route::get('/show/{id}', 'DopController@show')->name('show');//update page
                Route::post('/update/{id}', 'DopController@update')->name('update');//update
                //remove
                Route::get('/delete/{id}', 'DopController@delete')->name('delete');//remove
                //ban
                Route::get('/ban/{id}', 'DopController@ban')->name('ban');//banned
                //unban
                Route::get('/unban/{id}', 'DopController@unban')->name('unban');//unbanned
            });
        });
        Route::prefix('order')->name('order.')->group(function () {
            Route::get('/index', 'OrderController@index')->name('index');
            Route::get('/edit/{id}', 'OrderController@show')->name('edit');
            Route::post('/update/{id} ', 'OrderController@update')->name('update');
        });
    });

    Route::namespace('Client')->prefix('client')->name('client.')->middleware('Client')->group(function () {
        Route::prefix('order')->name('order.')->group(function () {
        });
    });
});
Route::namespace('Client')->prefix('client')->name('client.')->group(function ()  {
    Route::get('/index', 'CatalogController@index')->name('index');
    Route::get('/check', 'CatalogController@check')->name('check');
    Route::get('/pizza', 'CatalogController@pizza')->name('pizza');
    Route::get('/drink', 'CatalogController@drink')->name('drink');
    Route::get('/fetch_data', 'CatalogController@fetch_data')->name('fetch_data');
    Route::get('/search', 'CatalogController@search')->name('search');
    Route::get('/map', 'CatalogController@map')->name('map');
    Route::prefix('order')->name('order.')->group(function () {
        Route::post('/guest', 'OrderController@guest')->name('guest');
        Route::get('/payment', 'OrderController@payment')->name('payment');
        Route::post('/post', 'OrderController@post')->name('post');
        Route::get('/index', 'OrderController@index')->name('index');
        Route::post('/user', 'OrderController@user')->name('user');
        Route::get('/store', 'OrderController@store')->name('store');
        Route::post('/create', 'OrderController@create')->name('create');
        Route::get('/delete/{id}', 'OrderController@delete')->name('delete');
    });
});