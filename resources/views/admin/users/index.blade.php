@extends('layouts.app')
@section('content')

    <div class="container-fluid">
<style>
    table {width: 600px;}
    th {width: 20%;}
    td:first-child {width: 30%;}
</style>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <body>
                        <div class="container">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                        </div>

                        </body>
                        Admin redactor
                        <br>
                        <a class="btn btn-success"  href="{{ route('admin.users.create') }}"  type="submit">
                            {{ __('User create') }}
                        </a>
                        <table class="table table" >
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>name</th>
                                <th>phone</th>
                                <th>email</th>
                                <th>address</th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($admins as $key => $value)
                                <tr>
                                    {{ csrf_field() }}
                                    <td >{{ $value->id }}</td>
                                    <td>{{ $value->name }}</td>
                                    <td>{{ $value->phone }}</td>
                                    <td>{{ $value->email }}</td>
                                    <td>{{$value->address}}</td>
                                    <td>  <a class="btn btn-danger"  href="{{ route('admin.users.destroy',$value->id)}}" type="submit" name="' . $key . '">
                                            {{ csrf_field() }}{{ __('remove') }}  </a>
                                        <a class="btn btn-warning" href="{{ route('admin.users.show',$value->id)}}" type="submit" name="' . $key . '">
                                            {{('edit') }}  </a>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <input type="hidden" id="token" value="{{ csrf_token() }}">
                        {{ $admins->fragment('$admin')->links() }}
                            <div>Managers</div>

                            <br>
                            <table class="table table">
                                <thead>
                                <tr>
                                    <th>id</th>
                                    <th>name</th>
                                    <th>phone</th>
                                    <th>email</th>
                                    <th>address</th>
                                    <th></th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($managers as $key => $value)
                                    <tr>
                                        {{ csrf_field() }}
                                        <td >{{ $value->id }}</td>
                                        <td>{{ $value->name }}</td>
                                        <td>{{ $value->phone }}</td>
                                        <td>{{ $value->email }}</td>
                                        <td>{{$value->address}}</td>
                                        <td>  <a class="btn btn-danger"  href="{{ route('admin.users.destroy',$value->id)}}" type="submit" name="' . $key . '">
                                                {{ csrf_field() }}{{ __('remove') }}  </a>
                                            <a class="btn btn-warning" href="{{ route('admin.users.show',$value->id)}}" type="submit" name="' . $key . '">
                                                {{('edit') }}  </a>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <input type="hidden" id="token" value="{{ csrf_token() }}">
                            {{ $managers->fragment('$manager')->links() }}


                            <div>Clients</div>

                            <br>
                        <table class="table table">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>name</th>
                                <th>phone</th>
                                <th>email</th>
                                <th>address</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $key => $value)
                                <tr>
                                    {{ csrf_field() }}
                                    <td >{{ $value->id }}</td>
                                    <td>{{ $value->name }}</td>
                                    <td>{{ $value->phone }}</td>
                                    <td>{{ $value->email }}</td>
                                    <td>{{$value->address}}</td>
                                    <td>
                                        <a class="btn btn-danger"  href="{{ route('admin.users.destroy',$value->id)}}" type="submit" >
                                            {{ __('remove') }}  </a>
                                        <a class="btn btn-warning" href="{{ route('admin.users.show',$value->id)}}" type="submit">
                                            {{('edit') }}  </a>
                                        <a class="btn btn-primary" href="{{ route('admin.users.ban',$value->id)}}" type="submit" >
                                            {{('ban user') }}  </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <input type="hidden" id="2" value="{{ csrf_token() }}">
                        {{ $users->fragment('user')->links()  }}
                        <table class="table table">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>name</th>
                                <th>phone</th>
                                <th>email</th>
                                <th>address</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($bans as $key => $value)
                                <tr>
                                    {{ csrf_field() }}
                                    <td >{{ $value->id }}</td>
                                    <td>{{ $value->name }}</td>
                                    <td>{{ $value->phone }}</td>
                                    <td>{{ $value->email }}</td>
                                    <td>{{$value->address}}</td>
                                    <td>
                                        <a class="btn btn-danger"  href="{{ route('admin.users.destroy',$value->id)}}" type="submit" >
                                            {{ __('remove') }}  </a>
                                        <a class="btn btn-warning" href="{{ route('admin.users.show',$value->id)}}" type="submit">
                                            {{('edit') }}  </a>
                                        <a class="btn btn-primary" href="{{ route('admin.users.unban',$value->id)}}" type="submit" >
                                            {{('unban user') }}  </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <input type="hidden" id="2" value="{{ csrf_token() }}">
                        {{ $bans->fragment('ban')->links()  }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection