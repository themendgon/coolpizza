@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>
                    <div class="card-body">
                        <div class="container">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                        <body>
                        <div class="container">

                            <form method="post" action="{{ route('admin.users.update',$users->id)}}" >
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="name">Print name</label>
                                    <input class="form-control"  placeholder="{{$users->name}}" name="name" required>
                                </div>
                                <div class="form-group">
                                    <label for="email">Print email</label>
                                    <input class="form-control"  placeholder="{{$users->email}}" name="email" required>
                                </div>
                                <div class="form-group">
                                    <label for="password">Print password</label>
                                    <input class="form-control"  placeholder="password" name="password" required>
                                </div>
                                <div class="form-group">
                                    <label for="phone">Print phone</label>
                                    <input class="form-control"  placeholder="{{$users->phone}}" name="phone" required>
                                </div>
                                <div class="form-group">
                                    <label for="address">Print address</label>
                                    <input class="form-control"  placeholder="{{$users->address}}" name="address" required>
                                </div>
                                <div class="list-group">
                                    <select name='role' class="category_id">
                                        <option   value=0 >Admin</option>
                                        <option   value=1 >Manager</option>
                                        <option   value=2 >Client</option>
                                    </select>
                                </div>

                                <button type="submit" class="btn btn-default">update</button>
                            </form>
                        </div>
                        </body>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection