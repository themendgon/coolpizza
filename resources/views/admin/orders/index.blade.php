@extends('layouts.app')
@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <body>
                        <div class="container">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                        </div>

                        </body>

                        <br>
                        <table class="table table" >
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Products</th>
                                <th>Prise</th>
                                <th>Status</th>
                                <th>Name</th>
                                <th>Phone</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($all as $key => $value)
                                <tr>
                                    <td>Order {{$key}} </td>
                                    {{ csrf_field() }}
                                    <td>
                                    <ul>
                                    @foreach($value->list as $item)
                                            <li>{{ $item->product->title }}
                                                @if($item->product->pizza)
                                                @if( $item->size==1) 24
                                                @elseif($item->size==2) 30
                                                @else 35
                                                @endif
                                                (
                                                 @foreach($item->dop as $id)
                                                 {{$id->dop->title}}
                                                 @endforeach
                                                 )
                                                @endif</li>

                                        @endforeach
                                    </ul>
                                    </td>
                                    <td>{{$all[$key]->prise}}</td>

                                    <td>{{$all[$key]->status}}</td>
                                    <td>{{$all[$key]->user->name}}</td>
                                    <td>{{$all[$key]->user->phone}}</td>
                                    <td> <a class="btn btn-primary"  href="{{route('admin.order.edit',$all[$key]->id)}}" type="submit" >
                                            {{ __('Edit Status') }}  </a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection