@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>
                    <div class="card-body">
                        <div class="container">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                        <body>
                        <div class="container">

                            <form method="post" action="{{ route('admin.order.update',$post->id)}}" >
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="name">Print name</label>
                                    <input class="form-control"  placeholder="{{$post->status}}" name="status" required>
                                </div>
                                <button type="submit" class="btn btn-default">update</button>
                            </form>
                        </div>
                        </body>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection