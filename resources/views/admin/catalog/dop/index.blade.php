@extends('layouts.app')
@section('content')

    <div class="container-fluid">
        <style>
            table {width: 600px;}
            th {width: 20%;}
            td:first-child {width: 30%;}
        </style>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <body>
                        <div class="container">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                        </div>
                        <a href="{{route('admin.catalog.dop.create')}}" class="btn btn-primary">Create Dopping</a>
                        </body>
                        <table class="table table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Prise</th>

                                <th></th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($dop as $key => $value)
                                <tr>
                                    {{ csrf_field() }}
                                    <td >{{ $value->id }}</td>
                                    <td>{{ $value->title }}</td>
                                    <td>{{ $value->prise }}</td>
                                    <td>
                                        <a href="{{route('admin.catalog.dop.delete',$value->id)}}" class="btn btn-primary">Delete</a>
                                        <a href="{{route('admin.catalog.dop.show',$value->id)}}" class="btn btn-primary">Edit</a>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection