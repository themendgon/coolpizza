@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div>
                <a href="{{route('admin.catalog.dop.index')}}" class="btn btn-danger">Dopping</a>
            </div>
            <div class="col-md-10">
                <div class="">
                    <a href="{{route('admin.catalog.create')}}" class="btn btn-primary">Create pizza</a>
                </div>
                <div class="card">
                    <div class="card-header">Pizza</div>
                    <div class="card">
                        <div class="row">
                            @foreach($pizza as $key => $value)
                                {{ csrf_field() }}
                                <div class="col-sm-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title">{{$value->title}}</h5>
                                            <img src="{{ asset($value->image_url)}}">
                                            <p class="card-text">{{$value->parts}}</p>
                                            <span class="price">₽ {{$value->prise}}</span>
                                            <a href="{{route('admin.catalog.delete',$value->id)}}" class="btn btn-primary">Delete</a>
                                            <a href="{{route('admin.catalog.show',$value->id)}}" class="btn btn-primary">Edit</a>
                                            <a href="{{route('admin.catalog.ban',$value->id)}}" class="btn btn-primary">Add archive</a>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
                <br>
                <div class="card">
                    <div class="card-header">Drink</div>
                    <div class="card">
                        <div class="row">
                            @foreach($drink as $key => $value)
                                {{ csrf_field() }}
                            <div class="col-sm-4">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$value->title}}</h5>
                                        <img src="{{ asset($value->image_url)}}">
                                        <p class="card-text">{{$value->parts}}</p>
                                        <span class="price">₽ {{$value->prise}}</span>
                                        <a href="{{route('admin.catalog.delete',$value->id)}}" class="btn btn-primary">Delete</a>
                                        <a href="{{route('admin.catalog.show',$value->id)}}" class="btn btn-primary">Edit</a>
                                        <a href="{{route('admin.catalog.ban',$value->id)}}" class="btn btn-primary">Add archive</a>
                                    </div>
                                </div>
                                <br>
                            </div>
                         @endforeach

                        </div>
                    </div>
                </div>
                <br>
                <div class="card">
                    <div class="card-header">Deletes</div>
                    <div class="card">
                        <div class="row">
                            @foreach($deletes as $key => $value)
                                {{ csrf_field() }}
                                <div class="col-sm-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title">{{$value->title}}</h5>
                                            <img src="{{ asset($value->image_url)}}">
                                            <p class="card-text">{{$value->parts}}</p>
                                            <span class="price">₽ {{$value->prise}}</span>
                                            <a href="{{route('admin.catalog.delete',$value->id)}}" class="btn btn-primary">Delete</a>
                                            <a href="{{route('admin.catalog.show',$value->id)}}" class="btn btn-primary">Edit</a>
                                            <a href="{{route('admin.catalog.unban',$value->id)}}" class="btn btn-primary">Add catalog</a>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
