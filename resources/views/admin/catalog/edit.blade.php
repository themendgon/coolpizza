@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>
                    <div class="card-body">
                        <div class="container">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                        <body>
                        <div class="container">
                            <form action="{{ route('admin.catalog.update',$post->id) }}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="name">Print title</label>
                                    <input class="form-control"  placeholder={{$post->title}} name="title" required>
                                </div>
                                <div class="form-group">
                                    <label for="name">Print prise</label>
                                    <input class="form-control" type="number" placeholder={{$post->prise}} name="prise" required>
                                </div>
                                <div class="form-group">
                                    <label for="email">Print text</label>
                                    <input class="form-control"  placeholder={{$post->parts}} name="parts" required>
                                </div>
                                <div class="col-md-6">
                                    <input required type="file"  class="form-control" name="image">
                                </div>
                                <div class="col-md-10">Hot
                                    <input type="checkbox" id="hot" name="hot" value=1>
                                    Pizza
                                    <input type="checkbox" id="pizza" name="pizza" value=1>
                                    Vegetarian
                                    <input type="checkbox" id="vegetarian" name="vegetarian" value=1>
                                    Sale
                                    <input type="checkbox" id="sale" name="sale" value=1>
                                </div>
                                <button type="submit" class="btn btn-default">add</button>
                            </form>
                        </div>
                        </body>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection