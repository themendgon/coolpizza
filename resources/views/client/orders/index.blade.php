@extends('layouts.app')

@section('header')
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.2.0/mapbox-gl.js'></script>
    <script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.2.0/mapbox-gl-geocoder.min.js'></script>
    <link rel='stylesheet'
          href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.2.0/mapbox-gl-geocoder.css'
          type='text/css'/>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.2.0/mapbox-gl.css' rel='stylesheet'/>
    <style>
        #map {
            margin: 10px;
            width: 500px;
            height: 300px;
        }

        #instructions {
            margin: 20px;
            top: 0;
            padding: 20px;
            background-color: rgba(255, 255, 255, 0.9);
            font-family: sans-serif;
            line-height: 2em;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="container">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                        Order list
                        <br>
                        <table class="table table">
                            @foreach($products as $key => $value)
                                <tr>
                                    {{ csrf_field() }}
                                    <td>{{ $products[$key][0]->title }}
                                        @if($products[$key][0]->pizza==true)
                                            @if(session()->get($key)[1]==1)24
                                            @elseif(session()->get($key)[1]==2)30
                                            @else 35
                                            @endif
                                        @endif
                                    </td>
                                    <td>@foreach ($value as $item)
                                            {{$item->title}}
                                            {{$item->prise}},
                                        @endforeach</td>
                                    <td>
                                        <a class='btn-danger' href="{{route('client.order.delete',$key)}}">
                                            remove
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <div>
                           <h4> Prise order:{{session()->get('prise')}}</h4>
                        </div>
                        @guest
                            <form method="post" action="{{route('client.order.guest')}}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="name">Print name</label>
                                    <input class="form-control" name="name" required>
                                </div>
                                <div class="form-group">
                                    <label for="email">Print email</label>
                                    <input class="form-control" name="email">
                                </div>
                                <div class="form-group">
                                    <label for="phone">Print phone</label>
                                    <input class="form-control" name="phone" required>
                                </div>
                                <div class="form-group">
                                    <label for="address">Print address</label>
                                    <input class="form-control" name="address" required>
                                </div>
                                <div class="form-group">
                                    <label for="cash">Payment cash</label>
                                    <input type="checkbox" class="form-control" name="cash" value=1>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" class="form-control" id="lat" name="lat" required>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" class="form-control" id="lon" name="lon" required>
                                </div>
                                <p>Indicate the location on the map</p>
                                <div class="row">
                                    <div class="md-8" id='map'></div>
                                    <div class="md-8">
                                        <div id="instructions"></div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success">Create</button>
                            </form>

                        @else
                            <form method="post" action="{{route('client.order.user')}}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="cash">Payment cash</label>
                                    <input type="checkbox" class="form-control" name="cash" value=1>
                                </div>

                                <div class="form-group">
                                    <input type="hidden" class="form-control" id="lat" name="lat" required>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" class="form-control" id="lon" name="lon" required>
                                </div>
                                <p>Indicate the location on the map</p>
                                <div class="row">
                                    <div class="md-8" id='map'></div>
                                    <div class="md-8">
                                        <div id="instructions"></div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success">Create</button>
                            </form>
                        @endguest
                        <input type="hidden" id="token" value="{{ csrf_token() }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script>
        mapboxgl.accessToken = 'pk.eyJ1IjoidGhlbWVuZGdvbiIsImEiOiJjanozb25xbzkwNTh3M2NwYjJhajhrdncwIn0.vuaBje_t7m0UAA5FQQW_4A';
        var map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [73.377724, 54.986622],
            zoom: 12
        });

        var bounds = [[73.10, 54.866622], [73.60, 55.10]];
        // map.setMaxBounds(bounds);

        var canvas = map.getCanvasContainer();

        var start = [73.377724, 54.986622];
        var step = 0;
        var dot;

        function getRoute(end) {
            var url = 'https://api.mapbox.com/directions/v5/mapbox/cycling/' + start[0] + ',' + start[1] + ';' + end[0] + ',' + end[1] + '?steps=true&geometries=geojson&access_token=' + mapboxgl.accessToken;
            var req = new XMLHttpRequest();
            req.responseType = 'json';
            req.open('GET', url, true);
            req.onload = function () {
                var data = req.response.routes[0];
                if (step === 0) {
                    dot = data.geometry.coordinates;
                }
                var route = data.geometry.coordinates;
                var geojson = {
                    type: 'Feature',
                    properties: {},
                    geometry: {
                        type: 'LineString',
                        coordinates: route
                    }
                };
                if (map.getSource('route')) {
                    map.getSource('route').setData(geojson);
                } else {
                    map.addLayer({
                        id: 'route',
                        type: 'line',
                        source: {
                            type: 'geojson',
                            data: {
                                type: 'Feature',
                                properties: {},
                                geometry: {
                                    type: 'LineString',
                                    coordinates: geojson
                                }
                            }
                        },
                        layout: {
                            'line-join': 'round',
                            'line-cap': 'round'
                        },
                        paint: {
                            'line-color': '#3887be',
                            'line-width': 3,
                            'line-opacity': 0.8
                        }
                    });
                }
                var instructions = document.getElementById('instructions');
                var steps = data.legs[0].steps;
                console.log(data.duration / dot.length);
                var tripInstructions = [];
                for (var i = 0; i < steps.length; i++) {
                    // tripInstructions.push('<br><li>' + steps[i].maneuver.instruction) + '</li>';
                    instructions.innerHTML = '<br><span class="duration">Trip time: ' + Math.floor(data.duration / 60) + 'min</span>' + '<br><span class="distance">Trip distance: ' + Math.floor(data.distance / 1000) + 'km</span>';
                }

            };
            req.send();
        }

        map.on('load', function () {

            getRoute(start);

            map.addLayer({
                id: 'point',
                type: 'circle',
                source: {
                    type: 'geojson',
                    data: {
                        type: 'FeatureCollection',
                        features: [{
                            type: 'Feature',
                            properties: {},
                            geometry: {
                                type: 'Point',
                                coordinates: start
                            }
                        }
                        ]
                    }
                },
                paint: {
                    'circle-radius': 10,
                    'circle-color': '#3887be'
                }
            });

        });
        map.on('click', function (e) {
            var coordsObj = e.lngLat;
            canvas.style.cursor = '';
            var coords = Object.keys(coordsObj).map(function (key) {
                return coordsObj[key];
            });
            document.getElementById('lat').value = coords[0];
            document.getElementById('lon').value = coords[1];
            var end = {
                type: 'FeatureCollection',
                features: [{
                    type: 'Feature',
                    properties: {},
                    geometry: {
                        type: 'Point',
                        coordinates: coords
                    }
                }
                ]
            };
            if (map.getLayer('end')) {
                map.getSource('end').setData(end);
            } else {
                map.addLayer({
                    id: 'end',
                    type: 'circle',
                    source: {
                        type: 'geojson',
                        data: {
                            type: 'FeatureCollection',
                            features: [{
                                type: 'Feature',
                                properties: {},
                                geometry: {
                                    type: 'Point',
                                    coordinates: coords
                                }
                            }]
                        }
                    },
                    paint: {
                        'circle-radius': 10,
                        'circle-color': '#f30'
                    }
                });
            }
            getRoute(coords);
        });
    </script>
@endsection

