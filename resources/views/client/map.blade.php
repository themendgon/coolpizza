@extends('layouts.app')

@section('header')
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.2.0/mapbox-gl.js'></script>
    <script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.2.0/mapbox-gl-geocoder.min.js'></script>
    <link rel='stylesheet'
          href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.2.0/mapbox-gl-geocoder.css'
          type='text/css'/>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.2.0/mapbox-gl.css' rel='stylesheet'/>
    <style>
        #map {
            position: absolute;
            top: 0;
            bottom: 0;
            width: 100%;
        }
        #instructions {
            position: absolute;
            margin: 20px;
            top: 0;
            padding: 20px;
            background-color: rgba(255, 255, 255, 0.9);
            font-family: sans-serif;
            line-height: 2em;
        }
        #card{
            height: 600px;
            width: 100%;
        }
    </style>
@endsection
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body" id="card">

                                <div id='map'></div>
                                    <div id="instructions"></div>


                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
@section('javascript')
<script>
    mapboxgl.accessToken = 'pk.eyJ1IjoidGhlbWVuZGdvbiIsImEiOiJjanozb25xbzkwNTh3M2NwYjJhajhrdncwIn0.vuaBje_t7m0UAA5FQQW_4A';
    var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [73.377724, 54.986622],
        zoom: 12
    });

    var bounds = [[73.10, 54.866622], [73.60, 55.10]];
    map.setMaxBounds(bounds);

    var canvas = map.getCanvasContainer();

    var start = [73.377724, 54.986622];
    var step=0;
    var dot;
    var coords = [{{session()->get('lat')}}, {{session()->get('lon')}}];

    function getRoute(end) {
        var url = 'https://api.mapbox.com/directions/v5/mapbox/cycling/' + start[0] + ',' + start[1] + ';' + end[0] + ',' + end[1] + '?steps=true&geometries=geojson&access_token=' + mapboxgl.accessToken;
        var req = new XMLHttpRequest();
        req.responseType = 'json';
        req.open('GET', url, true);
        req.onload = function() {
            var data = req.response.routes[0];
            if(step===0)
            {dot=data.geometry.coordinates;
            }
            var route = data.geometry.coordinates;
            var geojson = {
                type: 'Feature',
                properties: {},
                geometry: {
                    type: 'LineString',
                    coordinates: route
                }
            };
            if (map.getSource('route')) {
                map.getSource('route').setData(geojson);
            } else {
                map.addLayer({
                    id: 'route',
                    type: 'line',
                    source: {
                        type: 'geojson',
                        data: {
                            type: 'Feature',
                            properties: {},
                            geometry: {
                                type: 'LineString',
                                coordinates: geojson
                            }
                           }
                    },
                    layout: {
                        'line-join': 'round',
                        'line-cap': 'round'
                    },
                    paint: {
                        'line-color': '#3887be',
                        'line-width': 3,
                        'line-opacity': 0.8
                    }
                });
            }
            var instructions = document.getElementById('instructions');
            var steps = data.legs[0].steps;
            for (var i = 0; i < steps.length; i++) {
                instructions.innerHTML = '<br><span class="duration">Trip duration: ' + Math.floor(data.duration / 60) + ' min 🚴 </span>' +'<br><span class="distance">Trip distance: ' + Math.floor(data.distance / 1000) + 'km</span>';
            }

        };
        req.send();
    }

    map.on('load', function() {

       getRoute(start);

        map.addLayer({
            id: 'point',
            type: 'circle',
            source: {
                type: 'geojson',
                data: {
                    type: 'FeatureCollection',
                    features: [{
                        type: 'Feature',
                        properties: {},
                        geometry: {
                            type: 'Point',
                            coordinates: start
                        }
                    }
                    ]
                }
            },
            paint: {
                'circle-radius': 10,
                'circle-color': '#3887be'
            }
        });
       var end = {
            type: 'FeatureCollection',
            features: [{
                type: 'Feature',
                properties: {},
                geometry: {
                    type: 'Point',
                    coordinates: coords
                }
            }
            ]
        };
        map.addLayer({
            id: 'end',
            type: 'circle',
            source: {
                type: 'geojson',
                data: {
                    type: 'FeatureCollection',
                    features: [{
                        type: 'Feature',
                        properties: {},
                        geometry: {
                            type: 'Point',
                            coordinates: coords
                        }
                    }]
                }
            },
            paint: {
                'circle-radius': 10,
                'circle-color': '#f30'
            }
        });
        getRoute(coords);

    });
    map.on('click', function() {
    });
</script>
@endsection