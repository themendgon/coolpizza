@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center">
            @include('client.catalog.data')

            <div class="col-md-11">
                <div class="card">
                    <div class="card-header">Drink
                        Sorted by:
                        @sortablelink('title')
                        @sortablelink('prise')
                        @sortablelink('created_at')</div>
                </div>
                    <div class="card">
                        <div class="row">
                            @foreach($drink as $key => $value)
                                {{ csrf_field() }}
                                <div class="col-sm-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <form  action="{{route('client.order.create')}}" method="POST" >
                                                {{ csrf_field() }}
                                                <h5 class="card-title">{{$value->title}}</h5>
                                                <input  type="hidden" name="drink" id="drink" value={{$value->id}}>
                                                <img src="{{ asset($value->image_url)}}" width="60%">
                                                <p class="card-text">{{$value->parts}}</p>
                                                <span class="price">₽ {{$value->prise}}</span>
                                                <button type="submit"  class="btn btn-primary">basket</button>
                                            </form>                                </div>
                                    </div>
                                    <br>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
    </div>
@endsection
