@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        @include('client.catalog.data')

        <div class="col-md-11">
            <div class="card">
                <div class="card-header">Pizza
                    Sorted by:
                @sortablelink('title')
                @sortablelink('prise')
                @sortablelink('created_at')</div>
                <div class="card">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row">
                        @foreach($pizza as $key => $value)
                        {{ csrf_field() }}
                        <div class="col-sm-4">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">{{$value->title}}</h5>
                                    <img src="{{ asset($value->image_url)}}" width="60%">
                                    <p class="card-text">{{$value->parts}}</p>
                                    <span class="price">₽ {{$value->prise}}</span>
                                    <button type="button" class="btn btn-primary" data-toggle="modal"  data-target="#exampleModal" data-whatever="{{$value->id}}" data-value="{{$value->prise}}">
                                        basket
                                    </button>
                                </div>
                            </div>
                            <br>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
            <br>
            <div class="card">
                <div class="card-header">Drink</div>
                <div class="card">
                    <div class="row">
                        @foreach($drink as $key => $value)
                        {{ csrf_field() }}
                        <div class="col-sm-4">
                            <div class="card">
                                <div class="card-body">
                                    <form  action="{{route('client.order.create')}}" method="POST" >
                                        {{ csrf_field() }}
                                        <h5 class="card-title">{{$value->title}}</h5>
                                        <input  type="hidden" name="drink" id="drink" value={{$value->id}}>
                                        <img src="{{ asset($value->image_url)}}" width="60%">
                                        <p class="card-text">{{$value->parts}}</p>
                                        <span class="price">₽ {{$value->prise}}</span>
                                        <button type="submit"  class="btn btn-primary">basket</button>
                                    </form>
                                </div>
                            </div>
                            <br>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <br>
        </div>
        </div>
    </div>
</div>

@include('client.catalog.dop')
@endsection
