@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            @include('client.catalog.data')
            <div class="col-md-11">
                <div class="card">
                    <div class="card-header" id="sites">Pizza
                        Sorted by:
                        @sortablelink('title')
                        @sortablelink('prise')

                        Hot :<input type="checkbox" value=0>
                        Vegetarian :<input type="checkbox" value=1>
                        Sale :<input type="checkbox" value=2>
                    </div>
                    <div class="card">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="row" id="collection">
                            @foreach($pizza as $key => $value)
                                {{ csrf_field() }}
                                <div class="col-sm-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title">{{$value->title}}</h5>
                                            <img src="{{ asset($value->image_url)}}" width="60%">
                                            <p class="card-text">{{$value->parts}}</p>
                                            <span class="price">₽ {{$value->prise}}</span>
                                            <button type="button" class="btn btn-primary" data-toggle="modal"  data-target="#exampleModal" data-whatever="{{$value->id}}" data-value="{{$value->prise}}">
                                                basket
                                            </button>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
let parametr=[0,0,0];
        $(document).ready(function(){
            $('#sites input').change(function() {
                if(this.checked) {
                    parametr[this.value]=1;
                    console.log(parametr);
                    $.ajax({
                        url: '{{ route('client.check') }}',
                        type: "GET",
                        data:{'data':parametr},
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        },

                        success: function (data) {
                                console.log(data);
                                let html='';
                            for (var key in data)
                            {
                                html+='<div class="col-sm-4">' +
                                    '<div class="card">' +
                                    '<div class="card-body">' +
                                    '<h5 class="card-title">'+
                                    data[key].title
                                    +'</h5>' +
                                    '<img src="http://coolpizza.ru/storage/image/'+data[key].image+'"  width="60%">' +
                                    '<p class="card-text">'+ data[key].parts +'</p>' +
                                    '<span class="price">₽ '+ data[key].prise+'</span>' +
                                    '<button type="button" class="btn btn-primary" data-toggle="modal"  data-target="#exampleModal" data-whatever="'+ data[key].id+'" data-value="'+ data[key].prise+'">'
                                +'basket'
                                +'</button>' +
                                    '</div>' +
                                    '</div>' +
                                    '<br>' +
                                    '</div>';

                                console.log( data[key]); // выводит и "eats" и "jumps"
                            }
                            $('#collection').html(html);




                        },
                        error: function (msg) {
                            alert('Ошибка');
                        }
                    });
                }
                else {
                    parametr[this.value]=0;
                    $.ajax({
                        url: '{{ route('client.check') }}',
                        type: "GET",
                        data:{'data':parametr},
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        },

                        success: function (data) {
                            console.log(data);
                            let html='';
                            for (var key in data)
                            {
                                html+='<div class="col-sm-4">' +
                                    '<div class="card">' +
                                    '<div class="card-body">' +
                                    '<h5 class="card-title">'+
                                    data[key].title
                                    +'</h5>' +
                                    '<img src="http://coolpizza.ru/storage/image/'+data[key].image+'"  width="60%">' +
                                    '<p class="card-text">'+ data[key].parts +'</p>' +
                                    '<span class="price">₽ '+ data[key].prise+'</span>' +
                                    '<button type="button" class="btn btn-primary" data-toggle="modal" data-whatever="'
                                    + data[key].id+
                                    '" data-target="#exampleModal">' +
                                    'basket' +
                                    '</button>' +
                                    '</div>' +
                                    '</div>' +
                                    '<br>' +
                                    '</div>';

                                console.log( data[key]); // выводит и "eats" и "jumps"
                            }
                            $('#collection').html(html);
                        },
                        error: function (msg) {
                            alert('Ошибка');
                        }
                    });
                }
            });

        });
    </script>
    @include('client.catalog.dop')
@endsection
