@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center">
            @include('client.catalog.data')
            <div class="col-md-11">
                <div class="card">
                    <div class="card-header">Pizza</div>
                    <div class="card">
                        <div class="row">
                            @foreach($pizza as $key => $value)
                                {{ csrf_field() }}
                                <div class="col-sm-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title">{{$value->title}}</h5>
                                            <img src="{{ asset($value->image_url)}}"  width="60%">
                                            <p class="card-text">{{$value->parts}}</p>
                                            <span class="price">₽ {{$value->prise}}</span>
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-whatever="{{$value->id}}" data-target="#exampleModal">
                                                basket
                                            </button>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    @include('client.catalog.dop')
@endsection
